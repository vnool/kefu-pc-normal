
var xml2js = require('xml2js');
var spawn = require('child_process').spawn;
 var fs = require('fs');
 
//var cmd = "   svn list -R --xml >> "+ __dirname+"/ver.list.txt";//path.join(__dirname, '../doc/WeChat_5.5.29_C1018.exe');//下载微信2.5

//cmd = "cmd \\s make.bat";
 
//shell.openExternal(cmd);




class SVN{


  make(CMD,  callback ){ 

    var output = "";


    callback.stdout = (x)=>{
      output +=(x);
    };

    callback.stderr = (x)=>{
      output = (x);
    };
    ;

     _process("svn", CMD, callback);



    function _process(command, args, cb) {
      var stdout = "",
        stderr = "";
      // console.log(command, args);
      var child = spawn(command, args);
      child.stdout.on("data", function appendData(data) {
        var out = data.toString();
        stdout += out;
        if (cb.stdout) {
          cb.stdout(out);
        }
      });
      child.stderr.on("data", function appendData(data) {
        var out = data.toString();
        stderr += out;
        if (cb.stderr) {
          cb.stderr(out);
        }
      });

      function parentExit(code, sig) {
        if (child.connected) {
          child.kill(sig);
        }
      }
      process.on('exit', parentExit);

      child.on('close', function childExit(code, sig) {
        process.removeListener('exit', parentExit);
        if (stderr.length === 0) {
          if (args.indexOf('--xml') > -1) {
            xml2js.parseString(stdout, {
                attrkey: "_attribute",
                charkey: "_text",
                explicitCharkey: true,
                explicitArray: false
              },
              function parse(err, result) {
                cb(null, result);
              }
            );
          } else {
            cb(null, stdout);
          }
        } else {
          cb(code, stderr);
        }
      });
      return child;
    }
  }//make
}//CLASS
module.exports = SVN;
