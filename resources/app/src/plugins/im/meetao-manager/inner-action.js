const electron = require('electron');
const ipcRenderer = electron.ipcRenderer;
require('../../common/request');
const Common = require('../../../common');
var userInfoSession;
var http=Common.SERVER_CONTENT_MONITOR;
class MeetaoAction{


}
MeetaoAction.crmMenuShow = (uid)=>{
	ipcRenderer.sendToHost('crm.conversation.changed', {userid:uid});
}

// MeetaoAction.updateUnread = (uid)=>{
//   ipcRenderer.sendToHost('unread.changed', {userid:uid});
// }


MeetaoAction.msgSave=(msgObj)=>{
   console.log(msgObj);
    var datas=msgObj.elems;
    msgObj.im_flag="";
    if(msgObj.isMe){
    	msgObj.fromAccount=userInfoSession.memberId;
    	msgObj.fromAccountNick= unescape(userInfoSession.crmName) ;
    	msgObj.im_flag="1";
    	
    }
    else{
    	msgObj.toAccount=userInfoSession.memberId;
    	msgObj.toAccountNick= unescape(userInfoSession.crmName) ;
    	
    }
   
    var newData={};
       for(var c=0;c<datas.length;c++){
        if(datas[c].type=="TIMFaceElem"){
             //表情
           // console.log(datas[c].type+","+datas[c].content.data );
           newData.im_type="face";
           newData.im_content=datas[c].content.data;

       }
       else if(datas[c].type=="TIMTextElem"){
       	newData.im_type="text";
         //文本
           // console.log(datas[c].type+","+datas[c].content.text );
           newData.im_content=datas[c].content.text;
       }
       else if(datas[c].type=="TIMImageElem"){
       		newData.im_type="image";
       		var imgs=datas[c].content.ImageInfoArray;
       		if(imgs.length>0){
       			newData.im_content="<img src=\""+imgs[0].url+"\">";
       			newData.im_file=imgs[0].url;
       		}
       		
            //图片
       }
       newData.im_from_uid=msgObj.fromAccount;
       newData.im_from_name=msgObj.fromAccountNick;
       newData.im_to_uid=msgObj.toAccount;
       newData.im_to_name=msgObj.toAccountNick;
       newData.im_flag=msgObj.im_flag;
       console.log(newData);
       EZ.query(http+"im/add",EZ.toQueryString(newData),function(res){
       	   console.log(res);
       	   if(res.tag!=0){
       	   	   if(msgObj.retryTimes==null || msgObj.retryTimes<=5){
       	   	   		setTimeout(function(){
       	   	   			if(msgObj.retryTimes==null){
       	   	   				msgObj.retryTimes=1;
       	   	   			}
       	   	   			else{
       	   	   				msgObj.retryTimes=msgObj.retryTimes+1;
       	   	   			}
       	   	   			MeetaoAction.msgSave(msgObj);
       	   	   		},10000);

       	   	   }
       	   	   
       	   }

       })  
       }
}
MeetaoAction.newMsgNotify=(msgObj)=>{
  
  ipcRenderer.sendToHost('unread.changed', {unread: 1});
  
}
MeetaoAction.webimFileSendSave=(msgObj)=>{

	var size=msgObj.size;
	var senderId,receiverId;
	var data={};
    if(size>1024)
    	size=(size/1024).toFixed(1)+"KB";
    else{
    	size=size+"B";
    }
    data.im_from_uid=msgObj.senderId;
       data.im_from_name=msgObj.senderId;
       data.im_to_uid=msgObj.receiverId;
       data.im_to_name=msgObj.receiverId;
       data.im_flag=msgObj.im_flag;
data.im_type="file";
       if(msgObj.senderId==userInfoSession.memberId){
    	data.im_from_uid=userInfoSession.memberId;
    	data.im_from_name= unescape(userInfoSession.crmName) ;
    	data.im_flag="1";
    }
    else{
    	data.im_to_name= unescape(userInfoSession.crmName) ;;
    	
    }
    data.im_content="<a href=\""+msgObj.downUrl+"\" target=\"_blank\">"+msgObj.name+ "&nbsp;&nbsp;"+size+"</a>";
    console.log(data);
     EZ.query(http+"im/add",EZ.toQueryString(data),function(res){
       	  console.log(res);
       	   if(res.tag!=0){
       	   	   if(msgObj.retryTimes==null || msgObj.retryTimes<=5){
       	   	   		setTimeout(function(){
       	   	   			if(msgObj.retryTimes==null){
       	   	   				msgObj.retryTimes=1;
       	   	   			}
       	   	   			else{
       	   	   				msgObj.retryTimes=msgObj.retryTimes+1;
       	   	   			}
       	   	   			MeetaoAction.webimFileSendSave(msgObj);
       	   	   		},10000);

       	   	   }
       	   	   
       	   }

       })  

}
MeetaoAction.setSessionInfo=(userInfo)=>{

   userInfoSession=userInfo;
   console.log(userInfoSession);
}

 module.exports = MeetaoAction;
