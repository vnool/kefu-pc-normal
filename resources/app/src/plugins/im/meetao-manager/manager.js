const Wechat = require('../../../main/controllers/wechat-manager/webview-wechat'); 
const MeetaoEvents =  require('./events');
const PATH = require('path'); 
const Common = require('../../../common');

class MeetaoManager {
	static instance(){

		if($('webview[wechatid="meetao"]').length < 1)
		{
			 var C =MeetaoManager.oneInstance =  new MeetaoManager();
	         C.create((web)=>{ 
				  	   C.events = new MeetaoEvents(web);
				  	   C.show();

				  	    C.HeartBeat();
				  	    setInterval(()=>{
				  	   	  C.HeartBeat();
				  	   },1*60*1000);
				  	   
		     });
	     }else{
	     	 MeetaoManager.oneInstance.show();
	     }
	}

    HeartBeat(){
    	var crmId = Common.globals('crmId');
    	var url = Common.SERVER_CRM+'api/login/addOrUpdateMeetaoService';
    	var data = {
    		crmId,
    		nickName:  localStorage['username'],
    		headImg: ""};

		$.ajax({  
			 type:'post',  
			 url :  url,  
			 dataType : 'json', 
			 data: data, 
			 success  : function(res) {  
			 	 
			 },  
			 error : function() {   
			      
			 }  
		 });

    }
 

    show(){
    	if($('webview[wechatid="meetao"]').length > 0){
			Wechat.hideAll();
			$('webview[wechatid="meetao"]').show();
			$('webview[wechatid="meetao"]').css('z-index', window.showWx_zindex++) ;
            
			return;
	    }
    }
	
	create(callback){
		//"342432" Common.globals("crmId")
		var auth = {"memberId":Common.globals('crmId'),"sdkAppid":"1400022322"};
		 this.signLogin(auth, (sign)=>{

		 	    if ($('webview[wechatid=meetao]').length > 0) {
		 	    	$('webview[wechatid=meetao]').reload();
		 	    	return;
		 	    }

		 	    var url = PATH.join(__dirname,'../im/index.html');
		 	    url +='?memberId='+auth["memberId"]+"&sign="+ sign+"&crmName="+escape(localStorage['username']);
 
	 			var html = Wechat.makeWebview( {
	 				'wechatid': 'meetao',
	 				'partition': 'persist:meetao',
	 				'url': url,
	 				'zindex': 10,
	 				'script': null,
	 				'extsettings': 'nodeintegration=yes '
	 			} );
	 			var web = $(html);
	 			web.removeAttr('preload');
	 			var webNode = this.webNode = web[0];

	 			$('#wechatbox').append(web);

		 	    callback( webNode) ;

		 });
		 
	}
 
 	signLogin(params, callback){

		$.ajax({  
			 type:'get',  
			 url :  "https://oauth.meetao.com/api/uc/auth/tls/sign.json?sdkAppid="+params["sdkAppid"]+"&memberId="+params["memberId"],  
			 dataType : 'json',  
			 success  : function(res) {  
			 	console.log(res);
			 	 if(res.tag!=0){
			 		 alert(res.errMsg);
			 		 return false;
			 	 }
			 	 else{
			 		 if(res.data.result.sign==null){
			 			 alert('服务器错误'); 
			 			 console.error(res);
			 			 return false;
			 		 }
			 		 else{
			 			 callback(res.data.result.sign);
			 			 return true;
			 		 }
			 	 }
			 },  
			 error : function() {  
			     alert('服务器错误'); 
			     return false;
			 }  
		 });

   }

}//class

module.exports = MeetaoManager;