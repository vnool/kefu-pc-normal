/*
 * @Author: DingChengliang
 * @Date:   2017-04-07 19:19:30
 * @Last Modified by:   DingChengliang
 * @Last Modified time: 2017-05-12 09:43:40
 * @Description: 微信事件响应

 运行在主页面上
 */
'use strict';
 
 
const electron = require('electron');
const Template = require('dee-template');
//const CRM = require('../main/message');

class MeetaoEvents {
	constructor(webview) {
		this.webview = webview;
      
       this.init();
	   Template.EventFactory.make(this.webview, 'message'); //定义事件名
	   SDK.wechat.load.trigger(this.webview); //触发一个load事件
       

		

	}
	destroy(){}
	init() {
		// this.webview.on('dom-ready', (e) => {
		// 	var code =`  const MeetaoAction = require('../meetao-manager/inner-action');
		//      	windows.crmMenuShow= function (uid){ 
		// 				    MeetaoAction.crmMenuShow(uid); 
		// 			    }`;
  //            this.webview.executeJavaScript(code, false, function(r) {
	            
	 //        });
  //       });
       this.webview.addEventListener('ipc-message', event => {
			if(event.channel.substr(0, 4) == 'crm.'){

				this.event4meetao(event.channel, event.args[0]);
				//触发消息事件 ,主要是指某个用户被点击的事件

			}else if(event.channel=='unread.changed'){
                this.eventUnread (event.args[0]);  
			}

		});
	}
	eventUnread(data){
		var numInit=Number($("#meetao").find('.unreadCount').html());
		var num=++numInit;
		$("#meetao").find('.unreadCount').show().html(num);

		this.webview.message.trigger({ channel:'im.unread.changed',  data: data } );//触发消息事件 
	}
	event4meetao(channel, data) {
		//crm.conversation.changed
 		var contact={
			customerID: data.userid,
			UserName: data.userid,
		}
		var myinfo ={
			User:{wechat:'MEETAO_'+ Common.globals("crmId")}
		};
		// CRM.message(channel, {
		// 	'contact': contact,
		// 	'myinfo': myinfo,
		// 	'other': ''
		// });

		//
		var data = {
					'contact': contact,
					'myinfo': myinfo,
					'other': ''
				};

		//alert('云客服事件: 某个人被点击');
		console.log(data);

		this.webview.message.trigger({ channel,  data } );//触发消息事件 ,主要是指某个用户被点击的事件

	}  

 
} //wechat maker
module.exports = MeetaoEvents;